---
title: "French given names per year per department"
author: "Lucas Mello Schnorr, Jean-Marc Vincent"
date: "October, 2022"
output:
  html_document:
    df_print: paged
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Contexte du problème
L'objectif de cette activité est de développer une méthode pour répondre à des questions à partir d'un jeu de données fourni.

Le jeu de données sera le fichier des prénoms établi par l'INSEE, car les question peuvent être graduelles de la fréquence d'un prénom à la répartition géographique des prénoms, jusqu'à des mesures d'homogénéité territoriales.

SOURCE: [https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip](https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip), 

Ce jeu de données a été choisi également car il a une structure simple et qu'il est suffisemment grand pour que le travail ne puisse se faire à la main.

On aura besoin de la bibliothèque _tidyverse_ pour ces analyses. 
Décompresser  _dpt2020_txt.zip_ pour obtenir **dpt2022.csv**). Lire ce fichier en R, (éventuellement charger le package  `readr).

## Download Raw Data from the website
```{r}
file = "dpt2022_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip",
	destfile=file)
}
unzip(file)
```
Check if your file is the same as in the first analysis (reproducibility)
```{r}
library(digest)
chemin_fichier <- "dpt2022.csv"
hash_md5 <- digest(file = chemin_fichier, algo = "md5")
print(hash_md5)
```
expected :
MD5 (dpt2022.csv) = 51aa7a3cd0ff01fe07b35816c83a0138

## Build the Dataframe from file

```{r}
library(tidyverse)

FirstNames <- read_delim("dpt2022.csv",delim=";")
```

Toutes les questions suivantes peuvent nécessiter des analyses préliminaires, ne pas hésiter à présenter votre travail de manière judicieuse en explicitant votre démarche d'analyse et rédiger le compte rendu comme un rapport scientifique. 

Questions: 
1. Choisir un prénom et analyser sa fréquence au cours du temps, comparer avec d'autres prénoms.
2. Donner, par genre, le prénom le plus attribué par année. Analyser l'évolution du prénom le plus fréquent.
3. Quels départements ont la plus forte variété de de prénoms ? 



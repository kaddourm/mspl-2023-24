---
title: "Visualizing French snow research stations"
author: "Jean-Marc Vincent, Lucas Leandro Nesi"
date: "1/11/2022"
output:
  html_document: default
  pdf_document: default
geometry: margin=1.5in, top=0.5in, bottom=0.5in
subtitle: A RStudio Demonstration and Statistical Analysis Draft
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

This document presents the fundamental aspects of RStudio markdown with data from French snow research stations.

## Useful shortcuts
This section keeps useful RStudio shortcuts 

* Control + Alt + i : Open R chunk
* Control + Alt + c : Execute R chunk
* Control + Enter : Execute line

## Data acquisition
The data is available [here](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=94&id_rubrique=32).
First, we want a simple dataset explaining the stations. It is available under the Documentation section "Liste des stations du réseau"
Download the CSV file (This [link](https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Nivo/postesNivo.csv)).


## Reading the data
We can use the function `read_csv` from `tidyverse`:

```{r, results=FALSE, message=FALSE}
library(tidyverse)
data <- read_csv("data/postesNivo.csv")
```
## Checking the data
We can print the data to see/understand all the columns:
```{r}
data
```

So the data has 5 columns:

* Latitude : The latitude of the Station
* Longitude : The longitude of the Station
* ID : A identifier for cross-reference between other datasets (This is important for the real snow data set!)
* Altitude : The altitude of the Station
* Nom : The name of the Station

We can select a column (for example, Altitude) using the following syntax:
```{r, include=FALSE}
data$Altitude
```

## Basic statistics on this dataset
We can compute basic statistics on this dataset, for example, we can get a summary of the altitude column:

```{r}
summary(data$Altitude)
```
Summary computes the same thing as:
```{r}
min(data$Altitude)
quantile(data$Altitude, 0.25)
median(data$Altitude)
mean(data$Altitude)
quantile(data$Altitude, 0.75)
max(data$Altitude)
```
## Simple visualizations

We can create simple visualizations with this data.

### Histogram

We can generate a simple histogram of the altitude with function `hist`:

```{r}
hist(data$Altitude)
```

### Boxplot
Another possibility is the boxplot, we can use the function `boxplot`:
```{r, fig.width=3, fig.height=4}
boxplot(data$Altitude, ylab="Altitude (meters)")
```

## A Map plot of the data

We can use more sophisticated libraries to plot the locations of the stations on a map.
We will use the packages `ggplot` and `maps`. *Do not worry* about ggplot functions; they will be covered in the following classes.
I want to show how useful Literate Programming is and some tricks in RStudio.

First we need to install a extra library for generating a map:
```{r, eval=FALSE}
if("maps" %in% rownames(installed.packages()) == FALSE){
install.packages("maps", repos = "https://cloud.r-project.org/")
}
```

It is then necessary to select the map of France using the function `map_data` of `maps`:
```{r}
france_map <- map_data("france")
```

And we can plot the map of France with all regions:
```{r, fig.width=7,fig.height=7}

ggplot(france_map, aes(x = long, y = lat), height=10) +
  geom_polygon(aes(group = group), fill="lightgray", color="gray") 
   
```

We can then add the stations of our other dataset, plotting them as black points.
Also, we can add Grenoble Latitude (45.1885) and Longitude (5.7245) for a reference and plot it as a red point.

```{r, fig.width=7,fig.height=7}
grenoble_data <- data.frame(Longitude=c(5.7245), Latitude=c(45.1885), Name=c("Grenoble"))
ggplot(france_map, aes(x = long, y = lat), height=10) +
  geom_polygon(aes(group = group), fill="lightgray", color="gray") +
  geom_point(data=data, aes(x = Longitude, y=Latitude), size=0.5) +
  geom_point(data=grenoble_data, aes(x=Longitude, y=Latitude), color="red")
   
```

Because there are some many stations, we can zoom a part of the map, the region near Grenoble, and add the names of the stations:

```{r, fig.width=7,fig.height=7}
grenoble_data <- data.frame(Longitude=c(5.7245), Latitude=c(45.1885), Name=c("Grenoble"))
ggplot(france_map, aes(x = long, y = lat), height=10) +
  geom_polygon(aes(group = group), fill="lightgray", color="gray")  +
  geom_point(data=data, aes(x = Longitude, y=Latitude), size=1) +
  geom_point(data=grenoble_data, aes(x=Longitude, y=Latitude), color="red") +
  coord_cartesian(xlim = c(5.2, 6.6), ylim=c(44.8, 45.5)) +
  geom_text(data=data, aes(x = Longitude, y=Latitude+0.01, label=Nom), size=3) +
  geom_text(data=grenoble_data, aes(x = Longitude, y=Latitude+0.01, label=Name), color="red", size=3)
   
```